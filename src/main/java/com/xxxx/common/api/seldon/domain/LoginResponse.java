package com.xxxx.common.api.seldon.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LoginResponse {
    private Status status;
    private Result result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Result {
        private String token;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Status {
        private int code;
        private String descr;
    }
}
