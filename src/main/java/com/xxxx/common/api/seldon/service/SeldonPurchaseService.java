package com.xxxx.common.api.seldon.service;

import com.xxxx.common.api.seldon.domain.api.*;

import java.util.Optional;


public interface SeldonPurchaseService {

    Optional<String> initToken();

    PurchaseResponse getPurchase(int reportId, Long seldonId, String etpId);

    PurchaseDocumentResponse getPurchaseDocument(int reportId, Long seldonId, String etpId);

    PurchaseProtocolResponse getPurchaseProtocol(int reportId, Long seldonId, String etpId);

    PurchaseProtocolDocumentResponse getPurchaseProtocolDocument(int reportId, Long seldonId, String etpId);

    PurchaseContractResponse getPurchaseContract(int reportId, Long seldonId, String etpId);
}
