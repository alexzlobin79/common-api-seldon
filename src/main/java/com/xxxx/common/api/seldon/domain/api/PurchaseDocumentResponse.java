package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PurchaseDocumentResponse {
    private final Status status;
    private final Result result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Result {
        @JsonProperty("purchasesdocuments")
        private final List<PurchaseDocument> purchaseDocuments;

        @JsonIgnoreProperties(ignoreUnknown = true)
        @Data
        public static class PurchaseDocument {
            @ApiModelProperty("Идентификатор закупки, по которой передаётся документаци")
            @JsonProperty("seldonID")
            private final int seldonId;
            private final List<Document> documents;

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Document {
                @ApiModelProperty("Идентификатор документа")
                private final int id;
                @ApiModelProperty("Наименование документа")
                private final String name;
                @ApiModelProperty("Расширение файла")
                private final String fileType;
                @ApiModelProperty("Размер файла в Байтах")
                private final String fileSize;
                @ApiModelProperty("Ссылка на скачивание документа с источника")
                private final String urlSource;
                @ApiModelProperty("Ссылка на скачивание документа с ресурсов Seldon")
                private final String urlSeldon;
            }
        }
    }

}
