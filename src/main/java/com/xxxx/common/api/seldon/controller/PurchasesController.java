package com.xxxx.common.api.seldon.controller;

import com.xxxx.common.api.seldon.domain.api.*;
import com.xxxx.common.api.seldon.service.SeldonPurchaseClientImpl;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/purchases")
public class PurchasesController {


    private final SeldonPurchaseClientImpl client;

    @GetMapping("/purchase")
    @ApiOperation(
            value = "Получение информации по закупке",
            notes = "Метод предназначен для получения мета - информации по закупке."
    )
    public PurchaseResponse getPurchase(
            @RequestParam(name = "reportId", defaultValue = "1") int reportId,
            @RequestParam(name = "seldonId", required = false) Long seldonId,
            @RequestParam(name = "etpId", required = false) String etpId
    ) {
        return client.getPurchase(reportId, seldonId, etpId);
    }

    @GetMapping("/document")
    @ApiOperation(
            value = "Получение документации по закупке",
            notes = "Метод предназначен для получения мета - информации по документации закупке."
    )
    public PurchaseDocumentResponse getPurchaseDocument(
            @RequestParam(name = "reportId", defaultValue = "1") int reportId,
            @RequestParam(name = "seldonId", required = false) Long seldonId,
            @RequestParam(name = "etpId", required = false) String etpId
    ) {
        return client.getPurchaseDocument(reportId, seldonId, etpId);
    }

    @GetMapping("/protocol")
    @ApiOperation(
            value = "Получение протоколов по закупке",
            notes = "Метод предназначен для получения мета - информации по протоколам, привязанным к закупке."
    )
    public PurchaseProtocolResponse getPurchaseProtocol(
            @RequestParam(name = "reportId", defaultValue = "1") int reportId,
            @RequestParam(name = "seldonId", required = false) Long seldonId,
            @RequestParam(name = "etpId", required = false) String etpId
    ) {
        return client.getPurchaseProtocol(reportId, seldonId, etpId);
    }

    @GetMapping("/protocolDocument")
    @ApiOperation(
            value = "Получение документов протоколов по закупке",
            notes = "Метод предназначен для получения документов по протоколам, привязанным к закупке."
    )
    public PurchaseProtocolDocumentResponse getPurchaseProtocolDocument(
            @RequestParam(name = "reportId", defaultValue = "1") int reportId,
            @RequestParam(name = "seldonId", required = false) Long seldonId,
            @RequestParam(name = "etpId", required = false) String etpId
    ) {
        return client.getPurchaseProtocolDocument(reportId, seldonId, etpId);
    }

    @GetMapping("/contract")
    @ApiOperation(
            value = "Получение информации по контракту",
            notes = "Метод предназначен для получения мета-информации по контракту."
    )
    public PurchaseContractResponse getPurchaseContract(
            @RequestParam(name = "reportId", defaultValue = "2") int reportId,
            @RequestParam(name = "seldonId", required = false) Long seldonId,
            @RequestParam(name = "etpId", required = false) String etpId
    ) {
        return client.getPurchaseContract(reportId, seldonId, etpId);
    }

}
