package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PurchaseResponse {
    private final Status status;
    private final Result result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Result {
        private final List<Purchase> purchases;

        @JsonIgnoreProperties(ignoreUnknown = true)
        @Data
        public static class Purchase {
            @ApiModelProperty("Идентификатор закупки в Селдон")
            @JsonProperty("SeldonId")
            private final long seldonId;
            @ApiModelProperty("Тип закупки в базе данных: 1 – ЕИС по 223 ФЗ, 2 – Государственные закупки (Источник – ЕИС по 44 ФЗ), 3 – Коммерческие закупки")
            private final int reportId;
            @ApiModelProperty("Идентификатор поискового фильтра")
            private final Integer filterId;
            @ApiModelProperty("Наименование поискового фильтра, по которому найдена закупка")
            private final String filterName;
            @ApiModelProperty("Номер закупки (источник)")
            private final String notificationNumber;
            @ApiModelProperty("Способ проведения закупки (Seldon)")
            private final ContractType contractType;
            @ApiModelProperty("Способ проведения закупки с источника")
            private final String sourceContractType;
            @ApiModelProperty("Ссылка на источник")
            private final String purchaseLink;
            @ApiModelProperty("Дата публикации закупки")
            private final String publishDate;
            @ApiModelProperty("Наименование закупки")
            private final String subject;
            @ApiModelProperty("НМЦК закупки")
            private final BigDecimal purchasePrice;
            @ApiModelProperty("валюта НМЦК закупки")
            private final Currency currency;
            @ApiModelProperty("Наименование торговой площадки")
            private final String epName;
            @ApiModelProperty("Ссылка на торговую площадку")
            private final String epLink;
            @ApiModelProperty("Статус закупки")
            private final PurchaseStatus status;
            @ApiModelProperty("Дата начала подачи заявок")
            private final String startDate;
            @ApiModelProperty("Дата окончания подачи заявок")
            private final String endDate;
            @ApiModelProperty("Дата изменения основных данных в извещении о закупке")
            private final String changeDate;
            @ApiModelProperty("Дата вскрытия конвертов")
            private final String openCoversDate;
            @ApiModelProperty("Дата начала рассмотрения заявок")
            private final String reviewStartDate;
            @ApiModelProperty("Дата окончания рассмотрения заявок")
            private final String reviewFinishDate;
            @ApiModelProperty("Дата проведения аукциона")
            private final String holdingDate;
            @ApiModelProperty("Дата подведения итогов")
            private final String totalDate;
            @ApiModelProperty("Место подачи заявок")
            private final String requestPlace;
            @ApiModelProperty("Порядок подачи заявок")
            private final String requestOrder;
            @ApiModelProperty("Организатор закупки")
            private final Organization organizer;
            @ApiModelProperty("Лоты закупки")
            private final List<Lot> lotsList;

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class ContractType {
                @ApiModelProperty("Код")
                private final int code;
                @ApiModelProperty("Наименование способа проведения закупки (Seldon)")
                private final String name;
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class PurchaseStatus {
                @ApiModelProperty("Код ЕИС")
                private final Integer codeStatusEIS;
                @ApiModelProperty("Статус ЕИС")
                private final String statusEIS;
                @ApiModelProperty("Код Селдон")
                private final int codeStatusSeldon;
                @ApiModelProperty("Статус Селдон")
                private final String statusSeldon;
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Lot {
                @ApiModelProperty("Идентификатор лота")
                private final int id;
                @ApiModelProperty("Номер лота")
                private final Long lotNumber;
                @ApiModelProperty("Наименование лота")
                private final String subject;
                @ApiModelProperty("Цена лота")
                private final BigDecimal price;
                @ApiModelProperty("Валюта цены лота")
                private final Currency currency;
                @ApiModelProperty("Номер позиции плана-графика")
                private final List<String> positionsPlanList;
                @ApiModelProperty("Идентификационный код закупки")
                @JsonProperty("IKZList")
                private final List<String> ikzList;
                @ApiModelProperty("Преимущества по лоту")
                private final List<String> preferencesList;
                @ApiModelProperty("Требования по лоту")
                private final List<String> requirementsList;
                @ApiModelProperty("Товары лота")
                private final List<Product> productsList;
                @ApiModelProperty("Заказчики лота")
                private final List<Customer> customersList;

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class Customer {
                    @ApiModelProperty("Информация о заказчике")
                    private final Organization organization;
                    @ApiModelProperty("Размер обеспечения заявки")
                    private final BigDecimal amount;
                    @ApiModelProperty("Валюта размера обеспечения заявки")
                    private final Currency amountCurrency;
                    @ApiModelProperty("Размер обеспечения контракта")
                    private final BigDecimal conAmount;
                    @ApiModelProperty("Валюта размера обеспечения контракта")
                    private final Currency conAmountCurrency;
                    @ApiModelProperty("Место поставки товара, выполнения работ, оказания услуг")
                    private final String deliveryPlace;
                    @ApiModelProperty("Срок поставки товара, выполнения работ, оказания услуг")
                    private final String deliveryTerm;
                }

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class Product {
                    @ApiModelProperty("Наименование товара (работы, услуги)")
                    private final String name;
                    @ApiModelProperty("Классификатор товара")
                    private final Classifier classifier;
                    @ApiModelProperty("Единица измерения")
                    private final Unit unit;
                    @ApiModelProperty("Цена за единицу товара")
                    private final BigDecimal price;
                    @ApiModelProperty("Количество данного товара")
                    private final BigDecimal quantity;
                    @ApiModelProperty("Полная стоимость данного товара")
                    private final BigDecimal value;

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    @Data
                    public static class Unit {
                        @ApiModelProperty("Код единицы измерения")
                        private final Integer code;
                        @ApiModelProperty("Наименование единицы измерения")
                        private final String name;
                    }

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    @Data
                    public static class Classifier {
                        @ApiModelProperty("Классификация по ОКДП")
                        private final Okdp okdp;
                        @ApiModelProperty("Классификация по ОКПД")
                        private final Okpd okpd;
                        @ApiModelProperty("Классификация по ОКПД2")
                        private final Okpd2 okpd2;

                        @JsonIgnoreProperties(ignoreUnknown = true)
                        @Data
                        public static class Okdp {
                            @ApiModelProperty("Код по ОКДП")
                            private final String code;
                            @ApiModelProperty("Наименование")
                            private final String name;
                        }

                        @JsonIgnoreProperties(ignoreUnknown = true)
                        @Data
                        public static class Okpd {
                            @ApiModelProperty("Код по ОКПД")
                            private final String code;
                            @ApiModelProperty("Наименование")
                            private final String name;
                        }

                        @JsonIgnoreProperties(ignoreUnknown = true)
                        @Data
                        public static class Okpd2 {
                            @ApiModelProperty("Код по ОКПД2")
                            private final String code;
                            @ApiModelProperty("Наименование")
                            private final String name;
                        }
                    }
                }

            }
        }
    }


}
