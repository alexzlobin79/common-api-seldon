package com.xxxx.common.api.seldon.service;

import com.xxxx.common.api.seldon.config.HttpProxyConfig;
import com.xxxx.common.api.seldon.config.SeldonApiConfig;
import com.xxxx.common.api.seldon.domain.LoginResponse;
import com.xxxx.common.api.seldon.domain.api.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpRequest;
import org.apache.http.*;
import org.apache.http.auth.AUTH;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.*;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.ssl.SSLContexts;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Optional;

import static org.apache.http.util.TextUtils.isBlank;


@Slf4j
@Component
public class SeldonPurchaseClientImpl implements SeldonPurchaseService {
    private static final String LOGIN_URI = "/user/Login";
    private static final String GET_PURCHASE_URI = "/purchases/get?token={token}";
    private static final String GET_PURCHASE_DOCUMENT_URI = "/purchasesdocuments/get?token={token}";
    private static final String GET_PURCHASE_PROTOCOL_URI = "/protocols/get?token={token}";
    private static final String GET_PURCHASE_PROTOCOL_DOCUMENT_URI = "/protocolsdocuments/get?token={token}";
    private static final String GET_PURCHASE_CONTRACT_URI = "/contracts/get?token={token}";

    private static String token;

    private final String loginUrl;
    private final String purchaseUrl;
    private final String purchaseDocumentUrl;
    private final String purchaseProtocolUrl;
    private final String purchaseProtocolDocumentlUrl;
    private final String purchaseContractlUrl;
    private final RestTemplate seldonRt;
    private final HttpEntity<MultiValueMap<String, String>> loginEntity;

    public SeldonPurchaseClientImpl(SeldonApiConfig seldonConfig, HttpProxyConfig httpProxyConfig) throws Exception {
        this.loginUrl = seldonConfig.getBaseUrl() + LOGIN_URI;
        this.purchaseUrl = seldonConfig.getBaseUrl() + GET_PURCHASE_URI;
        this.purchaseDocumentUrl = seldonConfig.getBaseUrl() + GET_PURCHASE_DOCUMENT_URI;
        this.purchaseProtocolUrl = seldonConfig.getBaseUrl() + GET_PURCHASE_PROTOCOL_URI;
        this.purchaseProtocolDocumentlUrl = seldonConfig.getBaseUrl() + GET_PURCHASE_PROTOCOL_DOCUMENT_URI;
        this.purchaseContractlUrl = seldonConfig.getBaseUrl() + GET_PURCHASE_CONTRACT_URI;

        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
        HttpClientBuilder httpClientBuilder = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .setDefaultCookieStore(new BasicCookieStore());

        if (httpProxyConfig.getEnabled()) {
            httpClientBuilder.setProxy(new HttpHost(httpProxyConfig.getHost(), httpProxyConfig.getPort()));

            if (httpProxyConfig.getUser() != null && httpProxyConfig.getPassword() != null) {
                CredentialsProvider credsProvider = new BasicCredentialsProvider();
                credsProvider.setCredentials(
                        new AuthScope(httpProxyConfig.getHost(), httpProxyConfig.getPort()),
                        new UsernamePasswordCredentials(httpProxyConfig.getUser(), httpProxyConfig.getPassword()));
                httpClientBuilder.setDefaultCredentialsProvider(credsProvider);

                if (httpProxyConfig.getPreemptiveBasic()) {
                    httpClientBuilder.setRequestExecutor(new HttpRequestExecutor() {
                        @Override
                        protected HttpResponse doSendRequest(
                                HttpRequest request, HttpClientConnection conn, HttpContext context
                        ) throws IOException, HttpException {
                            String auth = httpProxyConfig.getUser() + ":" + httpProxyConfig.getPassword();
                            byte[] encodedAuth = Base64.encodeBase64(
                                    auth.getBytes(StandardCharsets.ISO_8859_1));
                            request.addHeader(AUTH.PROXY_AUTH_RESP, "Basic " + new String(encodedAuth));
                            return super.doSendRequest(request, conn, context);
                        }
                    });
                }
            }
        }

        CloseableHttpClient httpClient = httpClientBuilder.build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

        seldonRt = new RestTemplate(requestFactory);

        DefaultUriTemplateHandler uth = new DefaultUriTemplateHandler();
        uth.setBaseUrl(seldonConfig.getBaseUrl());
        seldonRt.setUriTemplateHandler(uth);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("Name", seldonConfig.getLogin());
        map.add("Password", seldonConfig.getPassword());
        loginEntity = new HttpEntity<>(map, headers);
    }

    @Override
    @Scheduled(fixedDelay = 43200000)
    public Optional<String> initToken() {
        ResponseEntity<LoginResponse> response = seldonRt.postForEntity(loginUrl, loginEntity, LoginResponse.class);
        if (response == null) {
            log.warn("Get seldon api token response is empty");
            return Optional.empty();
        }

        if (!response.getStatusCode().equals(HttpStatus.OK)) {
            log.warn("Get seldon api token response is not valid, response status code is {}", response.getStatusCode().value());
            return Optional.empty();
        }

        LoginResponse loginResponse = response.getBody();
        if (HttpStatus.OK.value() != loginResponse.getStatus().getCode()) {
            log.warn("Get seldon api token response is not valid, login response status code is {} and reason is {}", loginResponse.getStatus().getCode(), loginResponse.getStatus().getDescr());
            return Optional.empty();
        }

        token = loginResponse.getResult().getToken();
        if (isBlank(token)) {
            log.warn("Get seldon api token response is not valid, login response does not contains token");
            return Optional.empty();
        }

        log.debug("Seldon api token refreshed");
        return Optional.of(token);
    }

    @Override
    public PurchaseResponse getPurchase(int reportId, Long seldonId, String etpId) {
        HttpEntity<MultiValueMap<String, String>> requestEntity = buildRequestEntity(reportId, seldonId, etpId);
        return seldonRt.postForEntity(purchaseUrl, requestEntity, PurchaseResponse.class, token).getBody();
    }

    @Override
    public PurchaseDocumentResponse getPurchaseDocument(int reportId, Long seldonId, String etpId) {
        HttpEntity<MultiValueMap<String, String>> requestEntity = buildRequestEntity(reportId, seldonId, etpId);
        PurchaseDocumentResponse response = seldonRt.postForEntity(purchaseDocumentUrl, requestEntity, PurchaseDocumentResponse.class, token).getBody();
        return response;
    }

    @Override
    public PurchaseProtocolResponse getPurchaseProtocol(int reportId, Long seldonId, String etpId) {
        HttpEntity<MultiValueMap<String, String>> requestEntity = buildRequestEntity(reportId, seldonId, etpId);
        return post(purchaseProtocolUrl, requestEntity, PurchaseProtocolResponse.class);
    }

    @Override
    public PurchaseProtocolDocumentResponse getPurchaseProtocolDocument(int reportId, Long seldonId, String etpId) {
        HttpEntity<MultiValueMap<String, String>> requestEntity = buildRequestEntity(reportId, seldonId, etpId);
        return post(purchaseProtocolDocumentlUrl, requestEntity, PurchaseProtocolDocumentResponse.class);
    }

    @Override
    public PurchaseContractResponse getPurchaseContract(int reportId, Long seldonId, String etpId) {
        HttpEntity<MultiValueMap<String, String>> requestEntity = buildRequestEntity(reportId, seldonId, etpId);
        return seldonRt.postForEntity(purchaseContractlUrl, requestEntity, PurchaseContractResponse.class, token).getBody();
    }

    private <T> T post(String url, HttpEntity entity, Class<T> clazz) {
        ResponseEntity<T> responseEntity = seldonRt.postForEntity(url, entity, clazz, token);
        return responseEntity.getBody();
    }

    private HttpEntity<MultiValueMap<String, String>> buildRequestEntity(int reportId, Long seldonId, String etpId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("reportId", String.valueOf(reportId));
        map.add("seldonId", String.valueOf(seldonId));
        map.add("etpId", etpId);
        return new HttpEntity<>(map, headers);
    }


}
