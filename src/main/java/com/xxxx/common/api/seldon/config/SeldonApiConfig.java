package com.xxxx.common.api.seldon.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "seldonApi")
public class SeldonApiConfig {
    private String baseUrl = "https://api.seldon2010.ru";
    private String login;
    private String password;
}
