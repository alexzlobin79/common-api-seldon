package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Status {
    private final Integer code;
    private final String descr;
}
