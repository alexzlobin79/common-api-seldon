package com.xxxx.common.api.seldon.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "http.proxy")
@Data
public class HttpProxyConfig {
    private Boolean enabled = Boolean.FALSE;
    private String host;
    private Integer port;
    private String user;
    private String password;
    private Boolean preemptiveBasic = Boolean.TRUE;
}
