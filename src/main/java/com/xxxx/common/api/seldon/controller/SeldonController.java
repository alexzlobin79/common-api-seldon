package com.xxxx.common.api.seldon.controller;

import com.xxxx.common.api.seldon.config.HttpProxyConfig;
import com.xxxx.common.api.seldon.config.SeldonBasisConfig;
import com.xxxx.common.api.seldon.domain.Bailiffs;
import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.*;
import org.apache.http.auth.AUTH;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.*;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.ssl.SSLContexts;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;

@RestController
@RequestMapping("/company")
@Slf4j
public class SeldonController {
    private static final String LOGIN = "/login";
    private static final String GET_COMPANIES_URI = "/get_my_companies";
    private static final String ADD_COMPANY_URI =
            "/add_to_my_companies?inn={inn}&ogrn={ogrn}";
    private static final String BAILIFFS_URI =
            "/get_{companyType}_bailiffs?inn={inn}&ogrn={ogrn}&orgnip={ogrnip}";
    private static final String COURT_CASESS_URI =
            "/get_{companyType}_court_cases?inn={inn}&ogrn={ogrn}&ogrnip={ogrnip}&type={type}" +
                    "&pageSize={pageSize}&pageIndex={pageIndex}&sortBy={sortBy}";
    private static final String GUARANTEES_URI =
            "/get_{companyType}_guarantees?inn={inn}&ogrn={ogrn}&ogrnip={ogrnip}";
    private static final String CONTRACTS_URI =
            "/get_{companyType}_contracts?inn={inn}&ogrn={ogrn}&ogrnip={ogrnip}&type={type}" +
                    "&pageSize={pageSize}&pageIndex={pageIndex}";
    private static final String CARD_URI =
            "/get_{companyType}_card?inn={inn}&ogrn={ogrn}&ogrnip={ogrnip}";

    private final RestTemplate seldonRt;
    private final HttpEntity<MultiValueMap<String, String>> loginEntity;

    public SeldonController(SeldonBasisConfig seldonConfig, HttpProxyConfig httpProxyConfig) throws Exception {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
        HttpClientBuilder httpClientBuilder = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .setDefaultCookieStore(new BasicCookieStore());
        if (httpProxyConfig.getEnabled()) {
            httpClientBuilder.setProxy(new HttpHost(httpProxyConfig.getHost(), httpProxyConfig.getPort()));

            if (httpProxyConfig.getUser() != null && httpProxyConfig.getPassword() != null) {
                CredentialsProvider credsProvider = new BasicCredentialsProvider();
                credsProvider.setCredentials(
                        new AuthScope(httpProxyConfig.getHost(), httpProxyConfig.getPort()),
                        new UsernamePasswordCredentials(httpProxyConfig.getUser(), httpProxyConfig.getPassword()));
                httpClientBuilder.setDefaultCredentialsProvider(credsProvider);

                if (httpProxyConfig.getPreemptiveBasic()) {
                    httpClientBuilder.setRequestExecutor(new HttpRequestExecutor() {
                        @Override
                        protected HttpResponse doSendRequest(
                                HttpRequest request, HttpClientConnection conn, HttpContext context
                        ) throws IOException, HttpException {
                            String auth = httpProxyConfig.getUser() + ":" + httpProxyConfig.getPassword();
                            byte[] encodedAuth = Base64.encodeBase64(
                                    auth.getBytes(StandardCharsets.ISO_8859_1));
                            request.addHeader(AUTH.PROXY_AUTH_RESP, "Basic " + new String(encodedAuth));
                            return super.doSendRequest(request, conn, context);
                        }
                    });
                }
            }
        }
        CloseableHttpClient httpClient = httpClientBuilder.build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

        seldonRt = new RestTemplate(requestFactory);

        DefaultUriTemplateHandler uth = new DefaultUriTemplateHandler();
        uth.setBaseUrl(seldonConfig.getBaseUrl());
        seldonRt.setUriTemplateHandler(uth);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("UserName", seldonConfig.getLogin());
        map.add("Password", seldonConfig.getPassword());
        loginEntity = new HttpEntity<>(map, headers);
    }

    @Scheduled(fixedDelay = 43200000)
    private void login() {
        seldonRt.postForEntity(LOGIN, loginEntity, String.class);
    }

    @GetMapping("/get_companies")
    @ApiOperation(
            value = "Компании, доступные для запросов",
            notes = "Метод возвращает ИНН и ОГРН компаний, по которым доступны запросы."
    )
    JsonNode getMyCompanies() {
        return seldonRt.getForObject(GET_COMPANIES_URI, JsonNode.class);
    }

    @GetMapping("/add_company")
    @ApiOperation(
            value = "Добавить компании, в список доступных для запросов",
            notes = "Метод добавляет компанию в список, по которым доступны запросы."
    )
    JsonNode addToMyCompanies(
            @RequestParam(required = false, defaultValue = "") String inn,
            @RequestParam(required = false, defaultValue = "") String ogrn
    ) {
        return seldonRt.getForObject(ADD_COMPANY_URI, JsonNode.class, inn, ogrn);
    }

    @GetMapping("/bailiffs")
    @ApiOperation(
            value = "Исполнительные производства",
            notes = "Метод возвращает данные об исполнительных производствах Федеральной службы судебных приставов в отношении компании."
    )
    Bailiffs bailiffs(
            @RequestParam(required = false, defaultValue = "") String inn,
            @RequestParam(required = false, defaultValue = "") String ogrn,
            @RequestParam(required = false, defaultValue = "") String ogrnip
    ) {
        String companyType = inn.length() == 10 || ogrn.length() == 13 ? "company" : "IP";
        return seldonRt.getForObject(BAILIFFS_URI, Bailiffs.class, companyType, inn, ogrn, ogrnip);
    }

    @GetMapping("/court_cases")
    @ApiOperation(
            value = "Арбитражные дела",
            notes = "Метод возвращает основные данные об арбитражных делах, в которых участвует компания."
    )
    JsonNode courtCases(
            @RequestParam(required = false, defaultValue = "") String inn,
            @RequestParam(required = false, defaultValue = "") String ogrn,
            @RequestParam(required = false, defaultValue = "") String ogrnip,
            @RequestParam(required = false, defaultValue = "4") Short type,
            @RequestParam(required = false, defaultValue = "100") Integer pageSize,
            @RequestParam(required = false, defaultValue = "1") Integer pageIndex
    ) {
        String companyType = inn.length() == 10 || ogrn.length() == 13 ? "company" : "ip";
        return seldonRt.getForObject(COURT_CASESS_URI, JsonNode.class,
                companyType,
                inn,
                ogrn,
                ogrnip,
                type,
                pageSize,
                pageIndex,
                "dateDesc"
        );
    }

    @GetMapping("/guarantees")
    @ApiOperation(
            value = "Банковские гарантии",
            notes = "Метод возвращает данные о банковских гарантиях, приобретенных компанией."
    )
    JsonNode guarantees(
            @RequestParam(required = false, defaultValue = "") String inn,
            @RequestParam(required = false, defaultValue = "") String ogrn,
            @RequestParam(required = false, defaultValue = "") String ogrnip
    ) {
        String companyType = inn.length() == 10 || ogrn.length() == 13 ? "company" : "ip";
        return seldonRt.getForObject(GUARANTEES_URI, JsonNode.class,
                companyType,
                inn,
                ogrn,
                ogrnip
        );
    }

    @GetMapping("/contracts")
    @ApiOperation(
            value = "Госконтракты. Список.",
            notes = "Метод возвращает основные данные об участии компании в госконтрактах."
    )
    public JsonNode contracts(
            @RequestParam(required = false, defaultValue = "") String inn,
            @RequestParam(required = false, defaultValue = "") String ogrn,
            @RequestParam(required = false, defaultValue = "") String ogrnip,
            @RequestParam(required = false, defaultValue = "1") String type,
            @RequestParam(required = false, defaultValue = "100") String pageSize,
            @RequestParam(required = false, defaultValue = "1") String pageIndex
    ) {
        String companyType = inn.length() == 10 || ogrn.length() == 13 ? "company" : "ip";
        return seldonRt.getForObject(CONTRACTS_URI, JsonNode.class,
                companyType,
                inn,
                ogrn,
                ogrnip,
                type,
                pageSize,
                pageIndex
        );
    }

    @GetMapping("card")
    public JsonNode card(
            @RequestParam(required = false, defaultValue = "") String inn,
            @RequestParam(required = false, defaultValue = "") String ogrn,
            @RequestParam(required = false, defaultValue = "") String ogrnip
    ) {
        String companyType = inn.length() == 10 || ogrn.length() == 13 ? "company" : "entrepreneur";
        return seldonRt.getForObject(CARD_URI, JsonNode.class,
                companyType,
                inn,
                ogrn,
                ogrnip
        );
    }

    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String httpClientErrorExceptionHandler(HttpClientErrorException exception) {
        log.warn("Seldon: {} {}", exception.getRawStatusCode(), exception.getResponseBodyAsString());
        if (exception.getRawStatusCode() == 403) login();
        return exception.getResponseBodyAsString();
    }
}
