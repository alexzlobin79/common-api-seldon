package com.xxxx.common.api.seldon.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "seldon")
public class SeldonBasisConfig {
    private String baseUrl = "https://basis.myseldon.com/api/rest";
    private String apiBaseUrl = "https://api.seldon2010.ru";
    private String login;
    private String password;
}
