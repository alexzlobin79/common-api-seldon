package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PurchaseProtocolDocumentResponse {
    private final Status status;
    private final Result result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Result {
        @JsonProperty("protocolsdocuments")
        public final List<ProtocolDocument> protocolsDocuments;

        @JsonIgnoreProperties(ignoreUnknown = true)
        @Data
        public static class ProtocolDocument {
            @ApiModelProperty("Идентификатор закупки в Селдон")
            @JsonProperty("seldonID")
            private final int seldonId;
            @ApiModelProperty("Тип закупки в базе данных")
            private final int reportId;
            @ApiModelProperty("Идентификатор поискового фильтра")
            private final int filterId;
            @ApiModelProperty("Наименование поискового фильтра, по которому найдена закупка")
            private final String filterName;
            @ApiModelProperty("Номер закупки (источник)")
            private final String notificationNumber;
            @ApiModelProperty("Наименование закупки")
            private final String subject;
            @ApiModelProperty("НМЦК закупки")
            private final BigDecimal purchasePrice;
            @ApiModelProperty("валюта НМЦК закупки")
            private final Currency currency;
            @ApiModelProperty("Протоколы закупки")
            private final List<Protocol> protocolsList;

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Protocol {
                @ApiModelProperty("Идентификатор протокола")
                @JsonProperty("Id")
                private final int id;
                @ApiModelProperty("Номер протокола")
                private final String number;
                @ApiModelProperty("Дата публикации протокола")
                private final String publishDate;
                private final List<Document> documents;

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class Document {
                    @ApiModelProperty("Идентификатор документа протокола")
                    private final int id;
                    @ApiModelProperty("Наименование документа протокола")
                    private final String name;
                    @ApiModelProperty("Тип файла")
                    private final String fileType;
                    @ApiModelProperty("Размер файла")
                    private final Long fileSize;
                    @ApiModelProperty("Ссылка на документ протокола")
                    private final String url;
                }
            }
        }
    }
}
