package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PurchaseProtocolResponse {
    private final Status status;
    private final Result result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Result {
        public final List<ProtocolResult> protocols;

        @JsonIgnoreProperties(ignoreUnknown = true)
        @Data
        public static class ProtocolResult {
            @ApiModelProperty("Идентификатор закупки в Селдон")
            @JsonProperty("seldonID")
            private final int seldonId;
            @ApiModelProperty("Тип закупки в базе данных")
            private final int reportId;
            @ApiModelProperty("Идентификатор поискового фильтра")
            private final int filterId;
            @ApiModelProperty("Наименование поискового фильтра, по которому найдена закупка")
            private final String filterName;
            @ApiModelProperty("Номер закупки (источник)")
            private final String notificationNumber;
            @ApiModelProperty("Наименование закупки")
            private final String subject;
            @ApiModelProperty("НМЦК закупки")
            private final BigDecimal purchasePrice;
            @ApiModelProperty("валюта НМЦК закупки")
            private final Currency currency;
            @ApiModelProperty("Лоты закупки")
            private final List<Lot> lotsList;
            @ApiModelProperty("Протоколы закупки")
            private final List<Protocol> protocolsList;

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Lot {
                @ApiModelProperty("Идентификатор лота")
                private final int id;
                @ApiModelProperty("Наименование лота")
                private final String subject;
                @ApiModelProperty("Цена лота")
                private final BigDecimal price;
                @ApiModelProperty("Валюта цены лота")
                private final Currency currency;
                @ApiModelProperty("Данные по заявкам из протоколов")
                private final App apps;

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class App {
                    @ApiModelProperty("Информация об организациях участников")
                    private final List<Organization> organizations;
                    @ApiModelProperty("Информация о заявках")
                    private final List<DataApp> dataApps;

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    @Data
                    public static class DataApp {
                        @ApiModelProperty("Номер заявки")
                        private final String appId;
                        @ApiModelProperty("Подробная информация о заявках")
                        private List<DataInfo> data;

                        @JsonIgnoreProperties(ignoreUnknown = true)
                        @Data
                        public static class DataInfo {
                            @ApiModelProperty("Статус допуска")
                            private final Boolean admitted;
                            @ApiModelProperty("Дата подачи заявки")
                            private final String appDate;
                            @ApiModelProperty("Оценка заявки в баллах")
                            private final BigDecimal evalResult;
                            @ApiModelProperty("Цена в заявке")
                            private final BigDecimal price;
                            @ApiModelProperty("Валюта цены лота")
                            private final Currency currency;
                            @ApiModelProperty("Результат рассмотрения заявки")
                            private final String resultType;
                            @ApiModelProperty("Идентификатор организации из блока organizations")
                            private final Integer organizationId;
                            @ApiModelProperty("Идентификатор протокола, содержащего заявку")
                            private final int protocolId ;
                        }
                    }
                }
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Protocol {
                @ApiModelProperty("Идентификатор протокола")
                @JsonProperty("Id")
                private final int id;
                @ApiModelProperty("Номер протокола")
                private final String number;
                @ApiModelProperty("Дата публикации протокола")
                private final String publishDate;
                @ApiModelProperty("Тип протокола")
                private final String type;
                @ApiModelProperty("Ссылка протокола")
                private final String href;
            }
        }
    }
}
