package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Currency {
    @ApiModelProperty("Код")
    private final String code;
    @ApiModelProperty("Наименование способа проведения закупки (Seldon)")
    private final String name;
}
