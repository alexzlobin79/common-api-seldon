package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PurchaseContractResponse {
    private final Status status;
    private final Result result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Result {
        private final List<Contract> contracts;

        @JsonIgnoreProperties(ignoreUnknown = true)
        @Data
        public static class Contract {
            @ApiModelProperty("Идентификатор закупки в Селдон")
            private final long seldonId;
            @ApiModelProperty("Тип раздела в базе данных: 4 - Контракты")
            private final int reportId;
            @ApiModelProperty("Идентификатор поискового фильтра")
            private final Integer filterId;
            @ApiModelProperty("Наименование поискового фильтра, по которому найдены контракты")
            private final String filterName;
            @ApiModelProperty("Номер контракта (источник)")
            private final String regNum;
            @ApiModelProperty("Тип данных о контракте (тип договора для данных по 223 ФЗ):  1–Договор, 2-Сведения о договоре ")
            private final Integer type;
            @ApiModelProperty("Ссылка на источник публикации контракта")
            private final String href;
            @ApiModelProperty("Статус контракта")
            private final String status;
            @ApiModelProperty("Дата подписания контракта")
            private final String signDate;
            @ApiModelProperty("Срок исполнения контракта")
            private final String executionDate;
            @ApiModelProperty("Цена контракта")
            private final BigDecimal contractPrice;
            @ApiModelProperty("Валюта цены контракта")
            private final Currency currency;
            @ApiModelProperty("Закупка, к которой привязан данный контракт")
            private final Purchase purchase;
            @ApiModelProperty("Заказчик контракта")
            private final Organization customer;
            @ApiModelProperty("Поставщики контракта")
            private final List<Organization> suppliers;
            @ApiModelProperty("Поставщики контракта")
            private final List<Product> productList;

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Purchase {
                @ApiModelProperty("Идентификатор закупки в Селдон")
                private final long seldonId;
                @ApiModelProperty("Тип закупки в базе данных: 1 – ЕИС по 223 ФЗ, 2 – Государственные закупки (Источник – ЕИС по 44 ФЗ), 3 – Коммерческие закупки")
                private final int reportId;
                @ApiModelProperty("Идентификатор закупки с источника (номер извещения о закупке с ЕИС)")
                private final String notificationNumber;
                @ApiModelProperty("Наименование закупки")
                private final String subject;
                @ApiModelProperty("НМЦК закупки")
                private final BigDecimal price;
                @ApiModelProperty("Валюта НМЦК закупки")
                private final Currency currency;
                @ApiModelProperty("Лоты закупки")
                private final Lot lot;

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class Lot {
                    @ApiModelProperty("Идентификатор лота в Селдон")
                    private final Long seldonId;
                    @ApiModelProperty("Наименование лота")
                    private final String subject;
                    @ApiModelProperty("Цена лота")
                    private final BigDecimal price;
                    @ApiModelProperty("Валюта цены лота")
                    private final Currency currency;
                }
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class Product {
                @ApiModelProperty("Наименование товара (работы, услуги)")
                private final String name;
                @ApiModelProperty("Классификатор товара")
                private final Classifier classifier;
                @ApiModelProperty("Единица измерения")
                private final Unit unit;
                @ApiModelProperty("Цена за единицу товара")
                private final BigDecimal price;
                @ApiModelProperty("Количество данного товара")
                private final BigDecimal quantity;
                @ApiModelProperty("Полная стоимость данного товара")
                private final BigDecimal value;

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class Unit {
                    @ApiModelProperty("Код единицы измерения")
                    private final Integer code;
                    @ApiModelProperty("Наименование единицы измерения")
                    private final String name;
                }

                @JsonIgnoreProperties(ignoreUnknown = true)
                @Data
                public static class Classifier {
                    @ApiModelProperty("Классификация по ОКДП")
                    private final Okdp okdp;
                    @ApiModelProperty("Классификация по ОКПД")
                    private final Okpd okpd;
                    @ApiModelProperty("Классификация по ОКПД2")
                    private final Okpd2 okpd2;

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    @Data
                    public static class Okdp {
                        @ApiModelProperty("Код по ОКДП")
                        private final String code;
                        @ApiModelProperty("Наименование")
                        private final String name;
                    }

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    @Data
                    public static class Okpd {
                        @ApiModelProperty("Код по ОКПД")
                        private final String code;
                        @ApiModelProperty("Наименование")
                        private final String name;
                    }

                    @JsonIgnoreProperties(ignoreUnknown = true)
                    @Data
                    public static class Okpd2 {
                        @ApiModelProperty("Код по ОКПД2")
                        private final String code;
                        @ApiModelProperty("Наименование")
                        private final String name;
                    }
                }
            }
        }
    }
}
