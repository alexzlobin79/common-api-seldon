package com.xxxx.common.api.seldon.domain.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Organization {
    @ApiModelProperty("ID в Seldon")
    private final int id;
    @ApiModelProperty("Уникальный номер организации с источника ЕИС")
    private final String idEIS;
    @ApiModelProperty("Наименование")
    private final String name;
    @ApiModelProperty("ИНН")
    private final String inn;
    @ApiModelProperty("КПП")
    private final String kpp;
    @ApiModelProperty("ОГРН по данным источника")
    private final String ogrnSource;
    @ApiModelProperty("ОГРН по данным Селдон")
    private final String ogrnSeldon;
    @ApiModelProperty("Код региона")
    private final String regionCode;
    @ApiModelProperty("Наименование региона")
    private final String region;
    @ApiModelProperty("Контактное лицо")
    private final String contactPerson;
    @ApiModelProperty("Уровень организации")
    private final Integer subOrdLevel;
    @ApiModelProperty("Юридический адрес")
    private final String urAddress;
    @ApiModelProperty("Фактический адрес")
    private final String factAddress;
    @ApiModelProperty("Телефоны")
    private final String phone;
    @ApiModelProperty("Email")
    private final String email;
}
