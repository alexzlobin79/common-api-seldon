package com.xxxx.common.api.seldon.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@SuppressWarnings("ALL")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Bailiffs {
    @ApiModelProperty("список дел")
    private final Case[] cases_list;
    private final Company company;
    private final Status status;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Case {
        private final String caseNum;
        private final String date;
        private final String document;
        private final Boolean isActive;
        private final String organ;
        private final String subject;
        private final BigDecimal sum;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Company {
        private final String fullName;
        private final String inn;
        private final String ogrn;
        private final String shortName;
        private final Status status;

        @JsonIgnoreProperties(ignoreUnknown = true)
        @Data
        public static class Status {
            private final Long code;
            private final String date;
            private final String name;
            private final RegOrgan regOrgan;

            @JsonIgnoreProperties(ignoreUnknown = true)
            @Data
            public static class RegOrgan {
                private final String code;
                private final String name;
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Status {
        private final Long itemsFound;
        private final String methodStatus;
        private final Boolean paramsAreValid;
    }
}
