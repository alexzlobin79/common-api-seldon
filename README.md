# Описание

Прокси-сервис к Seldon.BASIS API

- Spring Boot 1.5.4.RELEASE
- Spring Boot Actuator
- Springfox Swagger

# Сборка
```
mvn clean package
```
# Пример конфигурации
application.yml:
```yaml
server:
  port: 55555
spring:
  profiles:
    active: swagger
swagger:
  host: localhost:55555
seldon:
  baseUrl: https://api.myseldon.com/api/rest
  login: user123
  password: user123
seldonApi:
  baseUrl: https://api.seldon2010.ru
  login: user456
  password: user456
  
#Metrics related configurations
management:
  security:
    enabled: false  
```
**Для работы через прокси сервер** proxyHost по порту proxyPort [c аудентификацией по имени proxyUser и паролю proxyPassword] 
необходимо добавить:
```yaml
http:
  proxy:
    enabled: true
    host: proxyHost
    port: proxyPort
    user: proxyUser
    password: proxyPassword
```

#Метрики
Включены все дефолтные метрики Spring Boot и Prometheus:
```html
http://localhost:55555/actuator
http://localhost:55555/prometheus
```
