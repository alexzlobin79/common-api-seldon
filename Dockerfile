FROM java:8-jre-alpine
COPY target/common-api-seldon.jar service.jar
ENTRYPOINT java $JAVA_OPTS -jar /service.jar
EXPOSE 8080
